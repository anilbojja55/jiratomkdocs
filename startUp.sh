#!/bin/bash
[ -z "$username" ] && read -p "Username:" username
[ -z "$password" ] && read -s -p "Password:" password
echo ""
[ -z "$jiraurl" ] && read -p "Jira url like https://myawesomejira.net:" jiraurl
[ -z "$projectkey" ] && read -p "Project key like MYPROJ:" projectkey
java $PROXY -jar jira-to-mkdocs.jar -dir /workdir -jiraurl "$jiraurl" -username "$username" -password "$password" -projectkey "$projectkey"
echo Generating pages from markdown
( cd /workdir/mkdocs/$projectkey && mkdocs build )
echo Copying to $projectkey-site directory
mv /workdir/mkdocs/$projectkey/site /workdir/$projectkey-site