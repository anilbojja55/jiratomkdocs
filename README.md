

# Jiratomkdocs - Jira to mkdocs 
Jiratomkdocs but also jiratohtml and jiratomarkdown.
[jira2mkdocs / jira2html / jira2markdown]


## What is this for?

Jira project archiving for improved performance or when migrating to other solution.

This is a project that helps to store some details of Jira projects in Markdown and static HTML pages with search engine.

This project can be used when you no longer use some Jira projects and would like to remove them, but to be safe you would 
like to store some information for future in easily readable, accessible and searchable form. And removing additional data from Jira is beneficial as [explained here](https://www.atlassian.com/blog/jira-software/project-archiving-jira-software-enterprise).

**Attachments to issues are downloaded.**

### From this
![](img/jira.png)

### To this
![](img/mkdocs.png)


# Running
Just run the docker and provide parameters either through enviroment variables or when prompted. You will be prompted only for parameters you have not provided 
so for instance you can provide all parameters instead of password in enviroment variables and you will need to provide password when prompted for it so it will not be
visibile to people around you.

##### Example for Windows
```
docker run --rm -it --name jiratomkdocs ^
  -e jiraurl=https://yourname.atlassian.net ^
  -e username=yourusername ^
  -e projectkey=KEYOFYOURPROJECT ^
  -v %cd%:/workdir ^
  kaszaq/jiratomkdocs
```

##### Example for Unix
```
docker run --rm -it --name jiratomkdocs \
  -e jiraurl=https://yourname.atlassian.net \
  -e username=yourusername \
  -e projectkey=KEYOFYOURPROJECT \
  -v $(pwd):/workdir \
  kaszaq/jiratomkdocs
```

## Directory
You need to attach volume with local directory to `/workdir` inside container as shown in examples above. To this directory following directories will be created:
* `hfyag` - where all raw json issue data will be downloaded
* `mkdocs` - where all `markdown` files will be created. You can use these if you do not need html.
* `<KEYOFYOURPROJECT>-site` - directory for each project you wish to download

## How does this work

1. Connects to jira and downloads all issues using [howfastyouaregoing](https://bitbucket.org/kaszaq/howfastyouaregoing) library. When rerun projects will not be downloaded again from Jira as that data is already stored on local drive, unless removed intentionally.
2. Creates markdown files for every issue from given project.
3. Creates html static site using [mkdocs](https://www.mkdocs.org/) with [material design](https://squidfunk.github.io/mkdocs-material/)

#### Remote links
Remote links are not easily downloadable alongside issue data and hence are not provided through [howfastyouaregoing](https://bitbucket.org/kaszaq/howfastyouaregoing) library. 
This application loads remote links into directory `hfyag/jira/remotelinks` for caching purposes - to not redownloaded them on further reruns of this app. 
So if you are regenerating your site and think those links might have changed in the mean time you should clean up this directory.

#### Is this a backup?

**This is not a backup that you will be able to use later on to recreate your project in jira** - this can be done in jira itself. You should use this project when you would like to keep access
to easily readable format which you can peek into at any moment in future without requirement to have a jira.

Please keep in mind that this project **does not pull all data** regarding given project - like workflows or similar.


### Required parameters
* `jiraurl` - url to your jira, for instance to cloud jira it would something like `https://yourname.atlassian.net`
* `projectkey` - key of your project in Jira
Howfastyouaregoing app can run on one user account - all  information will be downloaded from Jira using that user credentials. Moreover project data will be automatically refreshed each run (unless triggered more often than 5 minutes).
* `username` - username of user to be used
* `password` - this is either password or [api token](https://confluence.atlassian.com/cloud/api-tokens-938839638.html)



### Other

* `PROXY` - set to "`-Dhttp.proxyHost=proxyaddress.here -Dhttp.proxyPort=80 -Dhttps.proxyHost=proxyaddress.here -Dhttps.proxyPort=443`" if need to use proxy to access jira

## License
Copyright 2018 Michał Kasza

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
```
http://www.apache.org/licenses/LICENSE-2.0
```
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

## Included project licenses
* [mkdocs](https://www.mkdocs.org/) [[BSD LICENSE]](https://www.mkdocs.org/about/license/#mkdocs-license-bsd)
    > Copyright © 2014, Tom Christie. All rights reserved.
    > 
    > Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
    > 
    > Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    > 
    > THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.