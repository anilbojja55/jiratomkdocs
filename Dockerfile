FROM python:2.7
RUN apt-get update && \
	apt-get install -y openjdk-8-jdk && \
	apt-get install -y ant && \
	apt-get clean;

RUN pip install mkdocs && \
	pip install mkdocs-material && \
	pip install pymdown-extensions

VOLUME /workdir

COPY startUp.sh startUp.sh
RUN chmod +x startUp.sh
COPY target/jira-to-mkdocs*.jar jira-to-mkdocs.jar

CMD ./startUp.sh