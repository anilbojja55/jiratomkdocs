/*
 * Copyright 2018 kaszaq.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.kaszaq.howfastyouaregoing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;

/**
 *
 * @author kaszaq
 */
public class CachingHttpClient {

    private final CloseableHttpClient httpClient;

    public CachingHttpClient(String username, String password) {
        String encoding = java.util.Base64.getEncoder().encodeToString((username + ":" + password).getBytes());

        httpClient = HttpClients.custom()
                .setDefaultHeaders(Stream.of(
                        new BasicHeader("Authorization", "Basic " + encoding)).collect(Collectors.toList()))
                .build();
    }

    public void download(File file, String url) throws IOException {

        if (file.exists()) {
            System.out.println("File already cached " + url);
            return;
        }
        HttpGet httpGet = new HttpGet(url);
        System.out.println("Executing request " + httpGet.getRequestLine());
        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            FileUtils.copyInputStreamToFile(response.getEntity().getContent(), file);
        }
    }
}
