package pl.kaszaq.howfastyouaregoing;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.ImmutableMap;
import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import static pl.kaszaq.howfastyouaregoing.Config.OBJECT_MAPPER;
import pl.kaszaq.howfastyouaregoing.agile.AgileClient;
import pl.kaszaq.howfastyouaregoing.agile.AgileClientFactory;
import pl.kaszaq.howfastyouaregoing.agile.AgileProject;
import pl.kaszaq.howfastyouaregoing.agile.AgileProjectProvider;
import pl.kaszaq.howfastyouaregoing.agile.Issue;
import pl.kaszaq.howfastyouaregoing.agile.jira.JiraAgileProjectProviderBuilderFactory;
import pl.kaszaq.howfastyouaregoing.utils.DateUtils;
import static pl.kaszaq.howfastyouaregoing.utils.DateUtils.parseDate;

public class Main {

    public static void main(String[] args) throws IOException {
        Options options = new Options();

        options.addOption("jiraurl", true, "url to jira instance");
        options.addOption("username", true, "username of user accessing jira");
        options.addOption("password", true, "can be either password or API-key");
        options.addOption("projectkey", true, "key of jira project, for instance MYPROJ");
        options.addOption("dir", true, "directory where to store jira project data");
        options.addOption("rawdata", "set this flag if you want to include raw issue data loaded from jira into pages");
        options.addOption("local", "set if the loaded data is already cached and you do not wish to check for update");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine;
        try {
            cmdLine = parser.parse(options, args);
        } catch (ParseException ex) {
            System.out.println("Unexpected exception:" + ex.getMessage());
            return;
        }
        if (!cmdLine.hasOption("projectkey")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar jira-to-mkdocs.jar", "projectkey option is required", options, "");
            return;
        }
        if (!cmdLine.hasOption("local")) {
            if (!cmdLine.hasOption("jiraurl")) {

                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("java -jar jira-to-mkdocs.jar", "jiraurl option is required", options, "");
                return;
            }
            if (!cmdLine.hasOption("username")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("java -jar jira-to-mkdocs.jar", "username option is required", options, "");
                return;
            }
            if (!cmdLine.hasOption("password")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("java -jar jira-to-mkdocs.jar", "password option is required", options, "");
                return;
            }
        }
        if (!cmdLine.hasOption("dir")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar jira-to-mkdocs.jar", "dir option is required", options, "");
            return;
        }

        final String projectKey = cmdLine.getOptionValue("projectkey");
        String directory = cmdLine.getOptionValue("dir");
        final File directoryFile = new File(directory);
        final File mkdocsDirectoryFile = new File(directoryFile, "mkdocs/" + projectKey);
        final File jiraDirectory = new File(directoryFile, "hfyag");
        final File jiraIssuesDirectory = new File(jiraDirectory, "jira/issues");
        final File jiraAttachmentsDirectory = new File(jiraDirectory, "jira/attachments");
        final File jiraRemotelinksDirectory = new File(jiraDirectory, "jira/remotelinks");
        final String username = cmdLine.getOptionValue("username");
        final String password = cmdLine.getOptionValue("password");
        CachingHttpClient client = new CachingHttpClient(username, password);
        final String jiraUrl = cmdLine.getOptionValue("jiraurl");
        AgileProjectProvider agileProjectProvider = JiraAgileProjectProviderBuilderFactory
                .withCredentials(username, password)
                .withCacheDir(jiraDirectory)
                .withJiraUrl(jiraUrl)
                .withCacheOnly(cmdLine.hasOption("local"))
                .withCustomFieldsParsers(ImmutableMap.of(
                        "comments", fieldsNode -> {
                            List<Comment> commentsList = new ArrayList();
                            fieldsNode.path("comment").path("comments").elements().forEachRemaining(e -> {
                                String name = e.path("author").path("name").asText();
                                String body = e.path("body").asText();
                                ZonedDateTime createdString = parseDate(e.path("created").textValue());
                                commentsList.add(new Comment(createdString, name, body));
                            });
                            return commentsList;
                        },
                        "attachments", fieldsNode -> {
                            List<Attachment> attachmentsList = new ArrayList();
                            fieldsNode.path("attachment").elements().forEachRemaining(e -> {
                                String filename = e.path("filename").asText();
                                String name = e.path("author").path("name").asText();
                                String id = e.path("id").asText();
                                String contentUrl = e.path("content").textValue();
                                //String thumbnailUrl = e.path("thumbnail").textValue();

                                File content = new File(jiraAttachmentsDirectory, id + "/" + filename);
                                try {
                                    client.download(content, contentUrl);
                                } catch (IOException ex) {
                                    throw new RuntimeException(ex);
                                }

                                ZonedDateTime createdString = parseDate(e.path("created").asText(null));

                                attachmentsList.add(new Attachment(createdString, name, filename, id));
                            });
                            return attachmentsList;
                        }
                ))
                .build();

        AgileClient agileClient = AgileClientFactory.newClient()
                .withAgileProjectProvider(agileProjectProvider)
                .create();

        System.out.println("Generating markdown files... ");

        AgileProject proj = agileClient.getAgileProject(projectKey);

        FileUtils.writeStringToFile(new File(mkdocsDirectoryFile, "mkdocs.yml"), "site_name: " + projectKey + " jira project backup\n"
                + "\n"
                + "theme:\n"
                + "  name: material\n"
                + "  palette:\n"
                + "    primary: 'blue'\n"
                + "  favicon: 'favicon.ico'\n"
                + "\n"
                + "  logo:\n"
                + "    icon: 'storage'\n"
                + "\n"
                + "extra_css:\n"
                + "  - 'stylesheets/extra.css'\n"
                + "markdown_extensions:\n"
                + "  - admonition\n"
                + "  - codehilite:\n"
                + "      guess_lang: false\n"
                + "      linenums: true\n"
                + "  - toc:\n"
                + "      permalink: true\n"
                + "  - pymdownx.betterem:\n"
                + "      smart_enable: all\n"
                + "  - pymdownx.critic\n"
                + "  - pymdownx.details\n"
                + "  - pymdownx.inlinehilite\n"
                + "  - pymdownx.magiclink\n"
                + "  - pymdownx.mark\n"
                + "  - pymdownx.smartsymbols\n"
                + "  - pymdownx.superfences\n"
                + "  - pymdownx.tasklist:\n"
                + "      custom_checkbox: true\n"
                + "  - pymdownx.tilde\n"
                + "nav:\n"
                + "    - Home: index.md", "UTF-8");
        SortedSet<Issue> issues = new TreeSet<>((o1, o2) -> {
            return o2.getUpdated().compareTo(o1.getUpdated());
        });
        issues.addAll(proj.getAllIssues());

        createMainPage(issues, mkdocsDirectoryFile, projectKey);
        copyExtraCss(mkdocsDirectoryFile);
        issues.parallelStream().forEach(issue -> {
            try {
                System.out.println("Generating page for issue: " + issue.getPrettyName());
                StringBuilder issuePage = new StringBuilder("# ");
                issuePage.append(issue.getKey())
                        .append(" ")
                        .append(issue.getSummary());
                StringBuilder issueHeader = new StringBuilder()
                        .append("\n\n**Type**: ")
                        .append(issue.getType())
                        .append("  ")
                        .append("\n\n**Created:** ")
                        .append(DateUtils.printDateTimeExcel(issue.getCreated()))
                        .append("  ")
                        .append("\n\n**Creator:** ")
                        .append(issue.getCreator())
                        .append("  ")
                        .append("\n\n**Status:** ")
                        .append(issue.getStatus())
                        .append("  ")
                        .append("\n\n**Resolution:** ")
                        .append(issue.getResolution())
                        .append("  ")
                        .append("\n\n**Updated:** ")
                        .append(DateUtils.printDateTimeExcel(issue.getUpdated()))
                        .append("  ");
                if (!issue.getLabels().isEmpty()) {
                    issueHeader.append("\n\n**Labels:** ")
                            .append(issue.getLabels().stream().map(s -> "==" + s + "==").collect(Collectors.joining(", ")))
                            .append("  ");
                }
                if (!issue.getComponents().isEmpty()) {
                    issueHeader.append("\n\n**Components:** ")
                            .append(issue.getComponents().stream().map(s -> "==" + s + "==").collect(Collectors.joining(", ")))
                            .append("  ");
                }
                List<Attachment> attachments = issue.get("attachments", new TypeReference<List<Attachment>>() {
                });
                if (!attachments.isEmpty()) {
                    issueHeader.append("\n\n**Attachments**\n\n"
                            + "|  |\n"
                            + "| --- |\n");

                    for (Attachment a : attachments) {
                        String fileStringLocation = "attachments/" + a.getId() + "/" + a.getFilename();
                        issueHeader.append("| [")
                                .append(a.getFilename())
                                .append("](")
                                .append(fileStringLocation)
                                .append(") - uploaded by @")
                                .append(a.getUsername())
                                .append(" on ")
                                .append(DateUtils.printDateTimeExcel(a.getDate()))
                                .append(" |\n");
                        final File dest = new File(mkdocsDirectoryFile, "docs/" + fileStringLocation);
                        final File from = new File(jiraAttachmentsDirectory, a.getId() + "/" + a.getFilename());
                        System.out.println("Copying attachment from " + from + " to " + dest);
                        FileUtils.copyFile(from, dest);
                    }
                    issueHeader.append("\n\n");
                }
                addRemoteLinks(issue, client, issueHeader, jiraRemotelinksDirectory, jiraUrl);

                if (issue.getParentIssueKey() != null) {
                    issueHeader.append("\n\n**Parent issue**\n\n")
                            .append("[ ")
                            .append(issue.getParentIssueKey())
                            .append("](")
                            .append(issue.getParentIssueKey())
                            .append(".md) ")
                            .append(Optional.ofNullable(proj.getIssue(issue.getParentIssueKey())).map(Issue::getSummary).orElse(""))
                            .append("  ");
                }
                if (!issue.getSubtaskKeys().isEmpty()) {
                    issueHeader.append("\n\n**Sub-tasks**\n\n"
                            + "| Key | Summary |\n"
                            + "| --- | --- |\n")
                            .append(issue.getSubtaskKeys().stream()
                                    .reduce("", (String l1, String l2) -> l1 + generateRow(proj, l2)));
                }
                if (!issue.getLinkedIssuesKeys().isEmpty()) {
                    issueHeader.append("\n\n**Linked issues**\n\n"
                            + "| Key | Summary |\n"
                            + "| --- | --- |\n")
                            .append(issue.getLinkedIssuesKeys().stream()
                                    .reduce("", (String l1, String l2) -> l1 + generateRow(proj, l2)));
                }

                issuePage.append("\n\n???+ info \"Info\"\n")
                        .append(addWhitespacesBeforeEachNewLine(issueHeader.toString(), 4));

                issuePage.append("\n\n???+ summary \"Description\"\n")
                        .append(addWhitespacesBeforeEachNewLine(J2M.changeToMarkdown(issue.getDescription()), 4));

                List<Comment> comments = issue.get("comments", new TypeReference<ArrayList<Comment>>() {
                });

                comments.forEach((comment) -> {
                    issuePage
                            .append("\n\n!!! quote \"")
                            .append("[").append(comment.getUsername()).append("] added a comment - ")
                            .append(DateUtils.printDateTimeExcel(comment.getDate()))
                            .append("\"\n\n")
                            .append(addWhitespacesBeforeEachNewLine(J2M.changeToMarkdown(comment.getText()), 4));
                });
                if (cmdLine.hasOption("rawdata")) {
                    issuePage.append("\n\n??? warning \"Raw data\"\n    ```json\n")
                            .append(addWhitespacesBeforeEachNewLine(OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(OBJECT_MAPPER.readTree(new File(jiraIssuesDirectory, issue.getKey() + ".json"))), 4))
                            .append("\n    ```");
                }
                FileUtils.writeStringToFile(new File(mkdocsDirectoryFile, "/docs/" + issue.getKey() + ".md"), issuePage.toString(), "UTF-8");
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }

        });

    }

    private static void createMainPage(SortedSet<Issue> issues, final File mkdocsDirectoryFile, final String projectKey) throws IOException {

        System.out.println("Generating main page");
        StringBuilder mainPage = new StringBuilder("# Issues of " + projectKey + "\n\n| Key | Summary | Last updated |\n"
                + "| --- | --- | --- |\n");
        issues.forEach(issue -> mainPage.append("| [").append(issue.getKey()).append("](").append(issue.getKey()).append(".md) | ").append(issue.getSummary().replaceAll("\\|", ":")).append(" | ").append(DateUtils.printSimpleDate(issue.getUpdated())).append(" |\n"));
        FileUtils.writeStringToFile(new File(mkdocsDirectoryFile, "/docs/index.md"), mainPage.toString(), "UTF-8");
    }


    private static String generateRow(AgileProject proj, String l2) {
        if (proj.getIssue(l2) != null) {
            return "| [" + l2 + "](" + l2 + ".md) | " + proj.getIssue(l2).getSummary().replaceAll("\\|", ":") + "|\n";
        } else {
            return "| " + l2 + " |  |\n";
        }
    }

    private static String addWhitespacesBeforeEachNewLine(String content, int i) {
        String prefix = StringUtils.repeat(" ", i);
        String[] lines = content.split("\\r?\\n");
        StringBuilder sb = new StringBuilder();
        for (String line : lines) {
            sb.append(prefix).append(line).append("\n");
        }
        return sb.toString();
    }

    private static void addRemoteLinks(Issue issue, CachingHttpClient client, StringBuilder issueHeader, File jiraRemotelinksDirectory, String jiraUrl) throws IOException {

        String fileStringLocation = issue.getKey() + ".json";
        String url = jiraUrl + "/rest/api/2/issue/" + issue.getKey() + "/remotelink";

        final File from = new File(jiraRemotelinksDirectory, fileStringLocation);
        client.download(from, url);
        JsonNode jsonNode = OBJECT_MAPPER.readTree(from);
        if (jsonNode.elements().hasNext()) {
            issueHeader.append("\n\n**Remote links**\n\n"
                    + "| |\n"
                    + "| --- |\n");
            jsonNode.elements().forEachRemaining(remoteLinkNode -> {
                String remoteLinkUrl = remoteLinkNode.path("object").path("url").asText();
                String remoteLinkTitle = remoteLinkNode.path("object").path("title").asText();
                issueHeader.append("| [")
                        .append(remoteLinkTitle)
                        .append("](")
                        .append(remoteLinkUrl)
                        .append(") - ")
                        .append(remoteLinkUrl)
                        .append(" |\n");
            });
            issueHeader.append(("\n\n"));
        }
    }

    private static void copyExtraCss(File mkdocsDirectoryFile) throws IOException {
        final File stylesheetsFile = new File(mkdocsDirectoryFile, "docs/stylesheets/extra.css");
        ClassLoader classLoader = Main.class.getClassLoader();
        FileUtils.copyInputStreamToFile(classLoader.getResourceAsStream("stylesheets/extra.css"), stylesheetsFile);
    }
}
